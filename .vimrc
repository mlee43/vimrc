set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

augroup Python
	autocmd!
	autocmd FileType python setlocal expandtab
augroup END

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"dracula theme
Plugin 'dracula/vim', { 'name': 'dracula' }

"toml syntax support
Plugin 'cespare/vim-toml'

"morrison commenting header BS
Plugin 'https://gitlab.com/mmorri22/vim-header.git'
let g:VimHeaderEmails = 'dwilli36@nd.edu'
let g:VimHeaderNames = 'Dane Williams'

"cool status bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

"vim settings everyone can agree on
Plugin 'tpope/vim-sensible'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


"fix tmux color issue
"set background = dark
set t_Co=256

syntax on

let g:dracula_italic = 0
let g:dracula_colorterm = 0
colorscheme dracula

filetype plugin on

set tabstop=4
set shiftwidth=4
set softtabstop=4

set autoindent
set smartindent
set cindent

" Automatically closing braces
"inoremap { {<CR>}<Esc>ko
"inoremap <silent> ( ()<ESC>hli

set mouse=a

set number

